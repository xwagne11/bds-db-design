CREATE TABLE "customer" (
  "customer_id" 		serial primary key not null,
  "customer_name" 		VARCHAR(40) not null,
  "website" 			VARCHAR(40)
);

CREATE TABLE "service_request" (
  "service_request_id" 		serial primary key not null,
  "sr_start" 			TIMESTAMP not null,
  "sr_end" 			TIMESTAMP,
  "customer_id" 		INT not null,
  foreign key ("customer_id") references "customer"
);

CREATE TABLE "category" (
  "category_id" 		serial primary key not null,
  "type" 			VARCHAR(20) not null
);

CREATE TABLE "asset_owner" (
  "owner_id" 			serial primary key not null,
  "given_name" 			VARCHAR(20) not null,
  "family_name" 		VARCHAR(20) not null,
  "mail" 			VARCHAR(40) not null,
  "logon_name" 			VARCHAR(30) not null
);

CREATE TABLE "location" (
  "location_id" 		serial primary key not null,
  "location" 			VARCHAR(64) not null
);

CREATE TABLE "asset" (
  "asset_id" 			serial primary key not null,
  "asset_name" 			VARCHAR(30) not null,
  "category_id" 		INT not null,
  "owner_id" 			INT not null,
  "location_id" 		INT not null,
  "customer_id" 		INT,
  foreign key ("category_id") references "category",
  foreign key ("owner_id") references "asset_owner",
  foreign key ("customer_id") references "customer",
  foreign key ("location_id") references "location"
);

CREATE TABLE "sr_involves_asset" (
  "asset_id" 			INT not null,
  "service_request_id" 		INT not null,
  foreign key ("service_request_id") references "service_request",
  foreign key ("asset_id") references "asset"
);

CREATE TABLE "mdm" (
  "mdm_id" 			serial primary key not null,
  "mdm_version" 		VARCHAR(20) not null
);

CREATE TABLE "project_type" (
  "project_type_id" 		serial primary key not null,
  "type" 			VARCHAR(20) not null
);

CREATE TABLE "project" (
  "project_id" 			serial primary key not null,
  "project_name" 		VARCHAR(30) not null,
  "project_type_id" 		INT,
  foreign key ("project_type_id") references "project_type"
);

CREATE TABLE "project_involves_asset" (
  "asset_id" 			INT not null,
  "project_id" 			INT not null,
  foreign key ("asset_id") references "asset",
  foreign key ("project_id") references "project"
);

CREATE TABLE "os" (
  "os_id" 			serial primary key not null,
  "os_name" 			VARCHAR(20) not null
);

CREATE TABLE "apns_certificate" (
  "apns_certificate_id" 	serial primary key not null,
  "isued_date" 			TIMESTAMP not null,
  "expire_date" 		TIMESTAMP not null
);

CREATE TABLE "server" (
  "server_id" 			serial primary key not null,
  "critical" 			BOOLEAN not null,
  "in_use" 			BOOLEAN not null,
  "os_id" 			INT not null,
  "asset_id" 			INT not null,
  "mdm_id" 			INT,
  "apple_integration" 		BOOLEAN not null,
  "apns_certificate_id" 	INT,
  foreign key ("mdm_id") references "mdm",
  foreign key ("os_id") references "os",
  foreign key ("apns_certificate_id") references "apns_certificate"
);

CREATE TABLE "incident_type" (
  "incident_type_id" 		serial primary key not null,
  "incident_name" 		VARCHAR(30) not null
);

CREATE TABLE "incidents" (
  "incident_id" 		serial primary key not null,
  "incident_start" 		TIMESTAMP not null,
  "incident_end" 		TIMESTAMP,
  "asset_id" 			INT not null,
  "incident_type_id" 		INT,
  foreign key ("asset_id") references "asset",
  foreign key ("incident_type_id") references "incident_type"
);

CREATE TABLE "customer_contact" (
  "customer_contact_id" 	serial primary key not null,
  "customer_id" 		INT not null,
  "name" 			VARCHAR(40) not null,
  "mail" 			VARCHAR(40),
  "phone" 			VARCHAR(20) not null,
  foreign key ("customer_id") references "customer"
);

